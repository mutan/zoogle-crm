import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { MenusComponent } from './menus/menus.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [MenusComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    ReactiveFormsModule
  ]
})
export class SettingsModule { }
