import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {

menu:FormGroup;
arrform:FormArray;

  constructor(private formBuilder:FormBuilder) {}
  
  ngOnInit() {
   this.menu=this.formBuilder.group({
    menuname:"",
     url:"",
     arrform: this.formBuilder.array([]),
   });
  }
  addCreds() {
    const creds = this.menu.controls.arrform as FormArray;
    creds.push(this.formBuilder.group({
      menuname: '',
      url: '',
    }));
  }
onsubmit(){
  console.log(this.menu.value);
}

}

